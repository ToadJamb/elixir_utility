defmodule UtilTest do
  use ExUnit.Case
  doctest Util

  @described_class Util

  describe ".type_of" do
    [
      [nil, :null],
      ["foo bar baz", :binary],
      [:foo, :atom],
      [7_123, :number],
      [7.123, :float],
      [true, :boolean],
      [%{ __struct__: Foo }, :struct],
      [%{ foo: "bar" }, :map],
      [[1, 2, 3], :list],
      [{"a", "b", 3}, :tuple],
      ['foo', :list],
      [Util, :module],
      [Foo, :atom],
    ] |> Enum.each(fn(args) ->
      [input, expected] = args
      params = Macro.escape(args)
      test "given #{inspect input}, it returns #{inspect expected}" do
        [input, expected] = unquote(params)
        assert @described_class.type_of(input) == expected
      end
    end)

    # functions don't like to be Macro.escape'd apparently
    test "given a function, it returns :function" do
      assert @described_class.type_of(fn(n) -> n end) == :function
    end
  end

  describe ".is_module?" do
    [
      [nil, false],
      ["foo bar baz", false],
      [:foo, false],
      [7_123, false],
      [7.123, false],
      [true, false],
      [%{ __struct__: Foo }, false],
      [%{ foo: "bar" }, false],
      [[1, 2, 3], false],
      [{"a", "b", 3}, false],
      ['foo', false],
      [Util, true],
      [Foo, false],
    ] |> Enum.each(fn(args) ->
      [input, expected] = args
      params = Macro.escape(args)
      test "given #{inspect input}, it returns #{inspect expected}" do
        [input, expected] = unquote(params)
        assert @described_class.is_module?(input) == expected
      end
    end)
  end

  describe ".is_type?" do
    [
      [["foo", :binary], true],
      [["foo", :module], false],
      [[true, :boolean], true],
      [[true, :atom], false],
    ] |> Enum.each(fn(args) ->
      [input, expected] = args
      params = Macro.escape(args)
      test "given #{inspect input}, it returns #{inspect expected}" do
        [input, expected] = unquote(params)
        actual = Kernel.apply(@described_class, :is_type?, input)
        assert actual == expected
      end
    end)
  end
end
