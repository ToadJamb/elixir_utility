defmodule Util do
  alias Util.IsIt
  alias Util.TypeOf

  @spec type_of(any) :: atom
  defdelegate type_of(var), to: TypeOf

  [
    :null,
    :binary,
    :boolean,
    :atom,
    :module,
    :float,
    :number,
    :struct,
    :map,
    :list,
    :tuple,
    :pid,
    :function,
    :unknown,
  ] |> Enum.each(fn(type) ->
    @spec unquote(:"is_#{type}?")(any) :: boolean
    defdelegate unquote(:"is_#{type}?")(var), to: IsIt
  end)

  @spec is_type?(any, atom) :: boolean
  defdelegate is_type?(var, type), to: IsIt
end
