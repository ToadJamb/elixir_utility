defmodule Util.IsIt do
  [
    :null,
    :binary,
    :boolean,
    :atom,
    :module,
    :float,
    :number,
    :struct,
    :map,
    :list,
    :tuple,
    :pid,
    :function,
    :unknown,
  ] |> Enum.each(fn(type) ->
    @spec unquote(:"is_#{type}?")(any) :: boolean
    def unquote(:"is_#{type}?")(var) do
      type = unquote(type)
      is_type? var, type
    end
  end)

  @spec is_type?(any, atom) :: boolean
  def is_type?(var, type), do: Util.type_of(var) == type
end
