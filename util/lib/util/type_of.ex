defmodule Util.TypeOf do
  @spec type_of(any) :: atom
  def type_of(nil),                       do: :null
  def type_of(var) when is_binary(var),   do: :binary
  def type_of(var) when is_boolean(var),  do: :boolean
  def type_of(var) when is_atom(var) do
    atom_type function_exported?(var, :__info__, 1)
  end
  def type_of(var) when is_float(var),    do: :float
  def type_of(var) when is_number(var),   do: :number
  def type_of(%{__struct__: _}),          do: :struct
  def type_of(%{}),                       do: :map
  def type_of(var) when is_list(var),     do: :list
  def type_of(var) when is_tuple(var),    do: :tuple
  def type_of(var) when is_pid(var),      do: :pid
  def type_of(var) when is_function(var), do: :function
  def type_of(_),                         do: :unknown

  @spec atom_type(boolean) :: :module | :atom
  defp atom_type(true),  do: :module
  defp atom_type(false), do: :atom
end
