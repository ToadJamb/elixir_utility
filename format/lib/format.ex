defmodule Format do
  alias Format.Colorize
  alias Format.Config
  alias Format.Mask

  @type maybe_atom :: atom | nil

  @spec replace_mask(String.t, list) :: String.t
  defdelegate replace_mask(mask, subs), to: Mask, as: :replace

  @spec colorize(any, atom) :: String.t
  defdelegate colorize(var, color), to: Colorize

  @spec colorize(any, atom, boolean) :: String.t
  defdelegate colorize(var, color, colorize?), to: Colorize

  @spec colorize_data(any) :: String.t
  defdelegate colorize_data(data), to: Colorize

  @spec colorize_data(any, boolean) :: String.t
  defdelegate colorize_data(var, colorize?), to: Colorize

  @spec colorize_type(atom) :: String.t
  defdelegate colorize_type(type), to: Colorize

  @spec colorize_type(atom, boolean) :: String.t
  defdelegate colorize_type(type, colorize?), to: Colorize

  @spec colorize? :: boolean
  defdelegate colorize?, to: Config

  @spec type_color(atom) :: maybe_atom
  defdelegate type_color(type), to: Config, as: :color_for
end
