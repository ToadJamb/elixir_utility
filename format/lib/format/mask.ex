defmodule Format.Mask do
  @spec replace(String.t, list) :: String.t
  def replace(mask, subs) do
    Enum.reduce subs, mask, &foo/2
  end

  defp foo(value, mask) do
    String.replace mask, "%s", to_string(value), global: false
  end
end
