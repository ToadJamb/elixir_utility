defmodule Format.Colorize do
  alias Format.Config

  @spec colorize(any, atom) :: String.t
  def colorize(var, color) when is_atom(color) do
    colorize var, color, Config.colorize?()
  end

  @spec colorize(any, atom, boolean) :: String.t
  def colorize(var, _color, false), do: plain(var)
  def colorize(var, color, true) when is_binary(var) and is_atom(color) do
    [color, var]
    |> IO.ANSI.format()
    |> to_string()
  end

  def colorize(var, color, true) when is_atom(color) do
    var
    |> colorize(color, false)
    |> colorize(color, true)
  end

  @spec colorize_type(atom) :: String.t
  def colorize_type(type) when is_atom(type),
    do: colorize_type(type, Config.colorize?())
  def colorize_type(type, false) when is_atom(type), do: plain(type)
  def colorize_type(type, true) when is_atom(type) do
    colorize type, Config.color_for(type), true
  end

  @spec colorize_data(any) :: String.t
  def colorize_data(data), do: colorize_data data, Config.colorize?()

  @spec colorize_data(any, boolean) :: String.t
  def colorize_data(data, false), do: inspect(data)
  def colorize_data(data, true) do
    data
    |> colorize_data(false)
    |> colorize(data
      |> Util.type_of()
      |> Config.color_for(),
      true
    )
  end

  @spec plain(any) :: String.t
  defp plain(value) when is_nil(value),    do: inspect(value)
  defp plain(value) when is_binary(value), do: value
  defp plain(value),                       do: to_string(value)
end
