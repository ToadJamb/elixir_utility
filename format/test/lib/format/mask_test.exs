defmodule MaskTest do
  use ExUnit.Case
  doctest Format.Mask

  @described_class Format

  describe ".replace_mask" do
    [
      [["foo", ["bar"]], "foo"],
      [["%s", ["bar"]], "bar"],
      [["%s:%s", ["foo", "bar"]], "foo:bar"],
    ] |> Enum.each(fn(args) ->
      [input, expected] = args
      params = Macro.escape(args)
      test "given #{inspect input}, it returns #{inspect expected}" do
        [input, expected] = unquote(params)
        actual = Kernel.apply(@described_class, :replace_mask, input)
        assert actual == expected
      end
    end)
  end
end
