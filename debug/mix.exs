defmodule Debug.MixProject do
  use Mix.Project

  def project do
    [
      app: :debug,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      dialyzer: dialyzer_options(),
      deps: deps(),
      aliases: aliases(),
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp aliases do
    [
      default: [
        "cmd mix compile --force",
        "cmd mix test --color",
        "cmd mix credo list --strict",
        "cmd mix dialyzer --halt-exit-status",
        "cmd mix utility.debug.show",
      ],
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},

      # 3rd-party packages
      {:logger_file_backend, "~> 0.0"},

      # maintained by me
      {:util, path: "../util"},
      {:format, path: "../format"},
      {:log, path: "../log"},

      # test/dev
      {:credo, "~> 0.9.1", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0.0-rc", only: [:dev, :test], runtime: false},
    ]
  end

  defp dialyzer_options do
    [plt_add_apps: [:mix]]
  end
end
