defmodule Debug do
  require Logger

  @spec show(any) :: any
  def show(data, description \\ "") do
    IO.puts format_message(data, description)
    log data, description
    data
  end

  @spec log(any, String.t) :: {:ok | :error, any}
  defp log(data, desc) do
    Logger.debug fn -> format_message(data, desc) end
  end

  @spec format_message(any, String.t) :: String.t
  defp format_message(data, desc) do
    "%s%s"
    |> Format.replace_mask([
      format_description(desc),
      format_data(data),
    ])
  end

  @spec format_description(nil | String.t) :: String.t
  defp format_description(""), do: ""
  defp format_description(nil), do: ""
  defp format_description(desc), do: "#{desc}: "

  @spec format_data(any) :: String.t
  defp format_data(data) do
    "%s :: %s"
    |> Format.replace_mask([
      Format.colorize_type(Util.type_of(data)),
      Format.colorize_data(data)
    ])
  end
end
