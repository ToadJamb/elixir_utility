defmodule Mix.Tasks.Utility.Debug.Show do
  use Mix.Task

  @shortdoc "example debug output with all data types"
  def run(_) do
    Mix.Task.run "app.start"

    basic_logging()

    Debug.show nil

    [
      nil,
      "foo bar baz",
      :foo,
      7_123,
      7.123,
      true,
      %{
        __struct__: Foo,
      },
      %{
        foo: "bar",
      }, [1, 2, 3],
      {"a", "b", 3},
      [foo: "bar", fizz: "buzz"],
      self(),
      fn -> IO.puts("hello") end,
      'foo',
    ] |> Enum.each(&(Debug.show(&1, "data")))
  end

  defp basic_logging do
    Log.debug "debug"
    Log.info "info"
    Log.warn "warn"
    Log.error "error"
  end
end
