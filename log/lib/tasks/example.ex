defmodule Mix.Tasks.Log.Example do
  use Mix.Task

  @shortdoc "example log output"
  def run(_) do
    Mix.Task.run "app.start"

    [
      nil,
      "foo bar baz",
      :foo,
      7_123,
      7.123,
      true,
      %{
        __struct__: Foo,
      },
      %{
        foo: "bar",
      }, [1, 2, 3],
      {"a", "b", 3},
      [foo: "bar", fizz: "buzz"],
      self(),
      fn -> IO.puts("hello") end,
      'foo',
      Foo,
    ] |> Enum.each(fn(data) ->
      Log.show data
      Log.show data, "plain"
      Log.show data, :warn
      Log.show data, "full", :debug
    end)

    basic_logging()
  end

  defp basic_logging do
    Log.debug "debug"
    Log.info fn -> "info" end
    Log.warn "warn"
    Log.error "error"
  end
end
