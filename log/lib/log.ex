defmodule Log do
  @levels Log.Logger.levels()

  @levels
  |> Enum.each(fn(level) ->
    @spec unquote(:"#{level}")(any) :: Log.Logger.log
    defdelegate unquote(:"#{level}")(message), to: Log.Logger
  end)

  @spec log(any) :: Log.Logger.log
  defdelegate log(msg), to: Log.Logger

  @spec log(any, any) :: Log.Logger.log
  defdelegate log(msg, lvl), to: Log.Logger

  @spec show(any) :: any
  defdelegate show(data), to: Log.DataLogger

  @spec show(any, String.t | :debug | :error | :info | nil | :warn) :: any
  defdelegate show(data, param), to: Log.DataLogger

  @spec show(any, String.t | nil, atom) :: any
  defdelegate show(data, msg, lvl), to: Log.DataLogger

  @spec format(atom, any, Log.Formatter.ts, list) :: String.t
  defdelegate format(lvl, msg, ts, meta), to: Log.Formatter
end
