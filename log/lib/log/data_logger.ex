defmodule Log.DataLogger do
  @levels Log.Logger.levels()

  # show/1
  @spec show(any) :: any
  def show(data), do: show data, nil

  # show/2
  @spec show(any, String.t | :debug | :error | :info | nil | :warn) :: any
  def show(data, lvl)
  when lvl in @levels,
  do: show data, nil, lvl
  def show(data, msg), do: show data, msg, :info

  # show/3
  @spec show(any, String.t | nil, atom) :: any
  def show(data, msg, lvl) do
    Log.log lvl, fn -> format_message(data, msg) end
    data
  end

  @spec format_message(any, String.t | nil) :: String.t
  defp format_message(data, desc) do
    "%s%s"
    |> Format.replace_mask([
      format_description(desc),
      format_data(data),
    ])
  end

  @spec format_description(nil | String.t) :: String.t
  defp format_description(""),   do: ""
  defp format_description(nil),  do: ""
  defp format_description(desc), do: "#{desc}: "

  @spec format_data(any) :: String.t
  defp format_data(data) do
    "%s :: %s"
    |> Format.replace_mask([
      Format.colorize_type(Util.type_of(data)),
      Format.colorize_data(data)
    ])
  end
end
