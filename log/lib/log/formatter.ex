defmodule Log.Formatter do
  alias Log.Config
  alias Logger.Formatter

  @masks %{
    :date  => "$date",
    :time  => "$time",
    :meta  => "$metadata",
    :level => "$level",
  }

  @type ts :: {
    {2_000..2_200, 1..12, 1..31},
    {0..24, 0..59, 0..59, 0..999},
  }

  @spec format(atom, any, ts, list) :: String.t
  def format(level, message, ts, meta) do
    [
      colorize_mask(:date),
      " ",
      colorize_mask(:time),
      " ",
      colorize_mask(:meta),
      "[",
      formatted_level(level),
      "] $levelpad$message\n"
    ] |> Enum.join()
    |> Formatter.compile()
    |> Formatter.format(level, message, ts, meta)
  rescue
    _ -> "Could not format: #{inspect {level, message, ts, meta}}\n"
  end

  @spec formatted_level(atom) :: String.t
  defp formatted_level(level) do
    colorize @masks[:level], level
  end

  @spec colorize_mask(atom) :: String.t
  defp colorize_mask(value) do
    colorize @masks[value], value
  end

  @spec colorize(any, atom) :: String.t
  defp colorize(value, color_key) do
    colorize value, color_key, Config.colorize?() and Format.colorize?()
  end

  @spec colorize(any, atom, boolean) :: String.t
  defp colorize(value, _key, false), do: Format.colorize(value, :na, false)
  defp colorize(value, key, true) do
    Format.colorize value, Config.color_for(key), true
  end
end
