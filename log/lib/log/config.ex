defmodule Log.Config do
  @type maybe_atom :: atom | nil
  @type maybe_map  :: map  | nil

  @colors %{
    :debug => :light_black,
    :info  => :light_blue,
    :warn  => :yellow,
    :error => :red,
    :date  => :light_black,
    :time  => :black,
    :meta  => :magenta,
  }

  @spec colors :: maybe_map
  def colors do
    :colors
    |> options_map_for()
    |> colors_from()
  end

  @spec color_for(atom) :: maybe_atom
  def color_for(key)
  when is_atom(key),
    do: find_value_for key, colors()

  @spec colorize? :: boolean
  def colorize?, do: is_map colors()

  @spec options :: map
  defp options, do: Application.get_env :log, :options, %{}

  @spec options_map_for(atom) :: any
  defp options_map_for(option) do
    options()
    |> Map.get(option, %{})
  end

  @spec colors_from(map) :: map
  defp colors_from(%{} = colors), do: Map.merge @colors, colors

  @spec colors_from(nil | atom | boolean | String.t) :: nil
  defp colors_from(_colors), do: nil

  @spec find_value_for(atom, map) :: any
  defp find_value_for(key, %{} = map), do: map[key]

  @spec find_value_for(atom, nil | atom | boolean | String.t) :: nil
  defp find_value_for(_, _), do: nil
end
