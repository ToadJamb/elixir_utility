defmodule Log.Logger do
  alias Log.Config

  @levels [
    :debug,
    :info,
    :warn,
    :error,
  ]

  @type log :: {:ok} | {:error, any}

  @spec levels :: list
  def levels, do: @levels

  @levels
  |> Enum.each(fn(level) ->
    @spec unquote(:"#{level}")(any) :: log
    def unquote(:"#{level}")(message), do: log unquote(:"#{level}"), message
  end)

  # log/1
  @spec log(any) :: log
  def log(msg), do: log msg, %{}

  # log/2
  @spec log(any, any) :: log
  def log(msg, opts = %{}), do: log :info, msg, opts

  def log(lvl, msg)
  when lvl in @levels,
  do: log lvl, msg, %{}

  def log(msg, lvl)
  when lvl in @levels,
  do: log lvl, msg, %{}

  # log/3
  @spec log(atom, any, map) :: log
  def log(level, msg, opts = %{})
  when level in @levels
    and (is_function(msg) or is_binary(msg))
  do
    opts
    |> Map.get(:module, Logger)
    |> Kernel.apply(:bare_log, [
      level,
      msg,
    ])
  end

  def log(level, msg, opts = %{})
  when level in @levels
  do
    message = [
      Format.colorize("Unexpected", Config.color_for(:error)),
      ": ",
      inspect(msg),
    ] |> Enum.join

    log level, message, opts
  end
end
