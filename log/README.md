# Log

Logging utilities to assist with color and formatting,
particularly when logging to files using `logger_file_backend`.


## Configuration

Custom colors may be configured as follows:

Use `:reset` to use default text color.

	config :log, :options, %{
		colors: %{
			:meta => :black,
		},
	}
