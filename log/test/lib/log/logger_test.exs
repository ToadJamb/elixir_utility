defmodule LogTest do
  use ExUnit.Case, async: true

  @described_class Log.Logger

  describe ".log" do
    [
      [[:info, "foo"], {:info, "foo"}],
      [[:info, {"foo"}], {
        :info, [
          Format.colorize("Unexpected", :red),
          ": ",
          "{\"foo\"}"
        ] |> Enum.join,
      }],
      [["foo"], {:info, "foo"}],
    ] |> Enum.each(fn(args) ->
      [input, expected] = args
      params = Macro.escape(args)
      test "given #{inspect input}, it returns #{inspect expected}" do
        [args, expected] = unquote(params)
        input = Enum.concat(args, [%{module: MockLogger}])

        actual = Kernel.apply(@described_class, :log, input)

        assert actual == expected
      end
    end)
  end
end
