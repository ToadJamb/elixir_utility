defmodule MockLogger do
  def bare_log(level, msg), do: {level, msg}
end
