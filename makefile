# Find mix.exs package files.
.PHONY: templates
.SILENT:

PACKAGES:=$(shell ls */mix.exs | xargs dirname) # | head -2)
DIR:=$(shell pwd)

define section
	$(call color_echo,3,================================================================================)
	$(call color_echo,3,' --- $(1) ---')
	$(call color_echo,3,================================================================================)
endef

define show
	$(call color_echo,12,--------------------------------------------------------------------------------); \
	printf ' - '; \
	$(call color_print,5,$(1)); \
	printf ' '; \
	$(call color_echo,10,$(2)); \
	$(call color_echo,12,--------------------------------------------------------------------------------)
endef

define color_echo
	tput setaf $(1); \
	echo $(2); \
	tput sgr0
endef

define color_print
	tput setaf $(1); \
	printf $(2); \
	tput sgr0
endef

define help
	tput setaf 2
	printf %-11s ' $(1) '
	tput sgr0

	printf :

	$(call color_echo,4,' $(2)')
endef

all:
	make help
	#make setup
	#make deps
	make default
	#make compile
	#make test
	#make credo
	#make dialyzer
	#make clean
	#make clean_all
	#make format
	#make templates

setup:
	asdf install
	make deps

help:
	$(call section,help)
	$(call help,help,show this message)
	$(call help,default,runs mix default for each project)
	$(call help,setup,installs dependencies)
	$(call help,deps,downloads project dependencies)
	$(call help,unused,cleans unused dependencies)
	$(call help,compile,compiles all projects)
	$(call help,test,runs tests for all projects)
	$(call help,credo,runs credo for all projects)
	$(call help,dialyzer,runs dialyzer for all projects)
	$(call help,clean,removes the _build folder)
	$(call help,clean_all,removes all temporary folders)
	$(call help,format,runs format for all projects)
	$(call help,templates,copies templates to each project)

deps:
	set -e; \
	$(call section,dependencies); \
	for pkg in $(PACKAGES); do \
		$(call show,dependencies,$$pkg); \
		cd $$pkg; \
		mix deps.get || exit; \
		mix deps.compile || exit; \
		cd $(DIR); \
	done

unused:
	set -e; \
	$(call section,dependencies); \
	for pkg in $(PACKAGES); do \
		$(call show,dependencies,$$pkg); \
		cd $$pkg; \
		mix deps.unlock --unused || exit; \
		mix deps.clean --unused || exit; \
		cd $(DIR); \
	done

default:
	set -e; \
	$(call section,dependencies); \
	for pkg in $(PACKAGES); do \
		$(call show,default,$$pkg); \
		cd $$pkg; \
		mix default || exit; \
		cd $(DIR); \
	done

compile:
	set -e; \
	$(call section,compiling); \
	for pkg in $(PACKAGES); do \
		$(call show,compiling,$$pkg); \
		cd $$pkg; \
		mix compile --force || exit; \
		cd $(DIR); \
	done

test:
	set -e; \
	$(call section,testing); \
	for pkg in $(PACKAGES); do \
		$(call show,testing,$$pkg); \
		cd $$pkg; \
		mix test || exit; \
		cd $(DIR); \
	done

credo:
	set -e; \
	$(call section,linting); \
	for pkg in $(PACKAGES); do \
		$(call show,linting,$$pkg); \
		cd $$pkg; \
		mix credo list --strict || exit; \
		cd $(DIR); \
	done

dialyzer:
	set -e; \
	$(call section,dialyzing); \
	for pkg in $(PACKAGES); do \
		$(call show,dialyzing,$$pkg); \
		cd $$pkg; \
		mix dialyzer --halt-exit-status || exit; \
		cd $(DIR); \
	done

clean:
	set -e; \
	$(call section,cleaning); \
	for pkg in $(PACKAGES); do \
		$(call show,cleaning,$$pkg); \
		cd $$pkg; \
		rm -rf _build || exit; \
		cd $(DIR); \
	done

clean_all:
	set -e; \
	$(call section,'cleaning all'); \
	for pkg in $(PACKAGES); do \
		$(call show,'cleaning all',$$pkg); \
		cd $$pkg; \
		rm -rf _build deps || exit; \
		cd $(DIR); \
	done

format:
	set -e; \
	$(call section,formatting); \
	for pkg in $(PACKAGES); do \
		$(call show,formatting,$$pkg); \
		cd $$pkg; \
		mix format || exit; \
		cd $(DIR); \
	done

templates:
	set -e; \
	$(call section,templating); \
	for pkg in $(PACKAGES); do \
		$(call show,templating,$$pkg); \
		cd $$pkg; \
		cp ../templates/.credo.exs config/; \
		cp ../templates/.formatter.exs .; \
		cd $(DIR); \
	done
